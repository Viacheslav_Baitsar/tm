package net.help.tm;

import net.help.tm.data.RequestData;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.mail.MessagingException;
import java.io.IOException;

import static net.help.tm.data.RequestData.*;

@Controller
public class IndexController {


    @GetMapping("/send")
    public String dat1(@ModelAttribute ("data")RequestData data) throws IOException, MessagingException {
        sendMail(data.email,data.HM);
        return "greeting";
    }
}