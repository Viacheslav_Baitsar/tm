package net.help.tm.data;

import javax.mail.MessagingException;
import java.io.IOException;

public class RequestData {
        public String email;
        public String HM;

    public RequestData(String email, String HM) {
        this.email = " @eu.fujikura.com";
        this.HM = "";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHM() {
        return HM;
    }

    public void setHM(String HM) {
        this.HM = HM;
    }

    public static void sendMail(String email, String HM) throws IOException, MessagingException {
        Mail.MakeMessage(email,HM);


    }
}

