package net.help.tm.data;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.List;

public class Photo {

    public static List<SmbFile> findPhoto(String dir, List<SmbFile> fileList, String NameOfFile) throws
            MalformedURLException, UnknownHostException, SmbException {
        String user = "bancl";
        String pass = "Fujikura01";

        NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("FAE", user, pass);
        SmbFile FileRootPath = new SmbFile(dir, auth);
        try {
            if (FileRootPath.isDirectory()) {
                System.out.println(FileRootPath);
                SmbFile[] directoryFiles = FileRootPath.listFiles();
                if (directoryFiles != null) {
                    for (SmbFile file : directoryFiles) {
                        if (file.isDirectory()) {
                            findPhoto(file.getPath(), fileList, NameOfFile);
                        } else {
                            if (file.getName().contains(NameOfFile)) {
                                fileList.add(file);
                            }
                        }
                    }
                }
            }
        } catch (SmbException e) {
            e.printStackTrace();
        }
        return fileList;
    }
}
