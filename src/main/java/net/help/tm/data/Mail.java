package net.help.tm.data;

import jcifs.smb.SmbFile;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import static javax.mail.Session.getDefaultInstance;
import static net.help.tm.data.Photo.findPhoto;

public class Mail {
    public static void MakeMessage(String mail, String HM) throws MessagingException, IOException, FileNotFoundException {
        final Properties properties = new Properties();
        FileInputStream in = new FileInputStream("mail.properties");
        properties.load(in);
        System.out.println(properties);
        Session mailSession = getDefaultInstance(properties);

        MimeMessage message = new MimeMessage(mailSession);
        message.setFrom(new InternetAddress("tm.eng.lviv2@gmail.com"));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(mail));
        message.setSubject("Photo_PVS");
        String dir = "smb://10.20.176.4/TestingData/8042/TestPhoto/";
        ArrayList<SmbFile> fileList = new ArrayList<>();
        findPhoto(dir, fileList, HM);
        String msg = "";
        if (fileList != null) {
            msg = "Hi, here your photo";
        } else {
            msg = "Hi, photo is absent";
        }
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(msg, "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);

        for (SmbFile file : fileList) {
            MimeBodyPart attachmentBodyPart = new MimeBodyPart();
            attachmentBodyPart.attachFile(new File(file.getPath().substring(4)));
            multipart.addBodyPart(attachmentBodyPart);
        }
        message.setContent(multipart);
        Transport tr = mailSession.getTransport();
        tr.connect("tm.eng.lviv2", "Expo2008@");
        //tr.sendMessage(message, message.getAllRecipients());
        tr.close();
    }
}
