package net.help.tm;

import jcifs.smb.SmbFile;
import net.help.tm.data.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;

@SpringBootApplication
public class TmApplication {
    public static void main(String[] args) throws IOException, MessagingException {
        SpringApplication.run(TmApplication.class, args);
        //RequestData.sendMail("den93q@gmail.com", "HM81553973");
    }

}
